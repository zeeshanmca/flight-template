<?php
   /**
   * @Author: Jahangir Ahmad
   * @Date:   2020-07-08 09:33:20
   * @Last Modified by:   Jahangir Ahmad
   * @Last Modified time: 2020-08-29 07:19:55
   */
   ?>
<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel='stylesheet' href='style.css'>
    <link rel='stylesheet' href='./Search/style.css'>
    <!-- <title>Catagories</title> -->
</head>

<body>
    <!-- Catagories -->
    <div id='wrapper'>
        <br>
        <h1>Browse Catagories
        </h1>
        <div style='text-align: center;'>
        </div>
        <div style='text-align: center;'>
            <!-- Testing Search -->
            <?php include( './Search/search.php' );?>
        </div>
        <br>
        <!-- Category Names -->
        <!-- Using loop to get category names from database -->
        <?php
         $sql = "SELECT * FROM categories";
         $result = mysqli_query( $conn, $sql );
         echo '<div class="toggles">';
         while ( $row = mysqli_fetch_assoc( $result )) {
             $cat = $row['category_name'];
             $id = $row['category_id'];

             echo '
                     <a href="./Threads/threads.php?catid='.$id.'"><button id="showall">'.$cat.'</button> </a>';
         }
         echo '</div>';
         
         ?>
        <!-- Category Container -->
        <!-- Use  loop to iterate through categories -->
        <div class='covers' style='text-align: center; padding-left: 20px;'>
            <!-- fetch all the categories -->
            <!-- Catagories -->
            <?php
         $sql = 'SELECT * FROM `categories`';
         $result = mysqli_query( $conn, $sql );
           while( $row = mysqli_fetch_assoc( $result ) )
           {
               $cat = $row['category_name'];
               $id = $row['category_id'];
               // echo $row['category_name'];
               echo '<div class="peopleskill cover">
                                       <div>
                                           <a href="./Threads/threads.php?catid='.$id.'">
                                               <img class="media-object" src="http://localhost/Forum/Categories/Images/card'.$id.'.jpg"
                                                   alt="Image for category">
                                               <h3>'.$cat.'</h3>
                                                   
                                           </a>
                                       </div>
                                   </div>';
           }
         ?>
         
</body>

</html>