<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-15 19:33:24
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-15 19:46:35
 */

?>
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <title>Code Slides</title> -->
    <link rel="stylesheet" href="new.css">
</head>
<body class="body">
    <div class="mainContent">
        <div class="content">
           <article class="topContent">
               <header>
                   <h2><a href="#" title="First Post">First Post</a></h2>
               </header>
               <footer>
                   <p class="post-info">This post is written by Jahangir</p>
               </footer>
               <content>
                   <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vel sapiente suscipit perspiciatis dolores incidunt delectus mollitia voluptates, aperiam fuga modi nulla, itaque quo repellendus harum. Obcaecati, similique eligendi. Quisquam, nulla?</p>
               </content>
           </article> 
           
           
           
           <article class="bottomContent">
            <header>
                <h2><a href="#" title="Second Post">Second Post</a></h2>
            </header>
            <footer>
                <p class="post-info">This post is written by Jahangir</p>
            </footer>
            <content>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vel sapiente suscipit perspiciatis dolores incidunt delectus mollitia voluptates, aperiam fuga modi nulla, itaque quo repellendus harum. Obcaecati, similique eligendi. Quisquam, nulla?</p>
            </content>
        </article> 

        <article class="topContent">
            <header>
                <h2><a href="#" title="Third Post">Third Post</a></h2>
            </header>
            <footer>
                <p class="post-info">This post is written by Jahangir</p>
            </footer>
            <content>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vel sapiente suscipit perspiciatis dolores incidunt delectus mollitia voluptates, aperiam fuga modi nulla, itaque quo repellendus harum. Obcaecati, similique eligendi. Quisquam, nulla?</p>
            </content>
        </article>


        <article class="topContent">
            <header>
                <h2><a href="#" title="Fourth Post">Fourth Post</a></h2>
            </header>
            <footer>
                <p class="post-info">This post is written by Jahangir</p>
            </footer>
            <content>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vel sapiente suscipit perspiciatis dolores incidunt delectus mollitia voluptates, aperiam fuga modi nulla, itaque quo repellendus harum. Obcaecati, similique eligendi. Quisquam Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae itaque suscipit optio voluptas sapiente expedita deleniti voluptate sit provident, saepe obcaecati, animi repellendus! At minima vero reiciendis cumque accusantium dolores!</p>
            </content>
        </article>
        
        </div>
    </div>
    <aside class="top-sidebar">
        <article>
            <h2>Top Sidebar</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod itaque assumenda cumque eius aliquam fugiat.</p>
        </article>
    </aside>
    <aside class="middle-sidebar">
        <article>
            <h2>Middle Sidebar</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod itaque assumenda cumque eius aliquam fugiat.</p>
        </article>
    </aside>
    <aside class="bottom-sidebar">
        <article>
            <h2>Bottom Sidebar</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod itaque assumenda cumque eius aliquam fugiat.</p>
        </article>
    </aside>
    <footer class="mainFooter">
        <p>Copyright &copy; <a href="#" title="Jahangir">Jahangir</a></p>
    </footer>
</body>
</html>