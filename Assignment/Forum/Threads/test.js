/*
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-10 10:05:51
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-10 10:05:53
 */
// ICONS CALLBACK
hicon.replace();

// SVGs
$("#svgDribbble").load(
    "https://s3-us-west-2.amazonaws.com/s.cdpn.io/373860/references.html #dribbble"
);
$("#svgTwitter").load(
    "https://s3-us-west-2.amazonaws.com/s.cdpn.io/373860/references.html #twitter"
);

// NEW CODE HERE

const moonPath =
    "M12 19C12 29.4934 19 37 19 38C8.50659 38 0 29.4934 0 19C0 8.50659 8.50659 0 19 0C19 0 12 8.50659 12 19Z";
const sunPath =
    "M38 19C38 29.4934 29.4934 38 19 38C8.50659 38 0 29.4934 0 19C0 8.50659 8.50659 0 19 0C29.4934 0 38 8.50659 38 19Z";

const darkMode = $(".mode");
// const darkMode = document.querySelector('#dark-mode');
let toggle = false;

// Set the behaviour
darkMode.click(function () {
    // Timeline set
    const timeline = anime.timeline({
        duration: 750,
        easing: "cubicBezier(.57,.19,.12,.99)"
    });

    // Animation
    timeline
        .add({
            targets: ".sun",
            d: [{ value: toggle ? sunPath : moonPath }]
        })
        .add(
            {
                targets: "#dark-mode",
                rotate: toggle ? 0 : -90
            },
            "-= 350"
        )
        .add(
            {
                targets: "#dark-mode",
                rotate: toggle ? 0 : -30
            },
            "-= 320"
        )
        .add(
            {
                targets: "html",
                background: toggle ? "rgb(241, 243, 249)" : "rgb(13, 13, 13)"
            },
            "-=2000"
        );

    // Conditions
    if (!toggle) {
        toggle = true;
    } else {
        toggle = false;
    }

    // Dark Colours Toggle
    if (!toggle) {
        $(".card").removeClass("card-d");
        $(".top").removeClass("top-d");
    } else {
        $(".card").addClass("card-d");
        $(".top").addClass("top-d");
    }
});
