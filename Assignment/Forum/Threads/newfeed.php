
<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-18 16:37:37
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-13 13:54:13
 */
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../_partials/style.css">

    <!-- <title>Document</title> -->
</head>

<body>
    <?php include('../_partials/nav.php');?>

    <!-- DataBase Connect -->
    <?php include('../_partials/_dbconnect.php');?>

    <!-- Getting Category Name and description -->
    <?php
    $id = $_GET['catid'];
     $sql = "SELECT * FROM catagories WHERE category_id= $id ";
     $result = mysqli_query( $conn, $sql );
    //  echo '<div class="toggles">';
     while ( $row = mysqli_fetch_assoc( $result ) ) {
         $catname = $row['category_name'];
         $catdesc = $row['category_description'];

     }

     ?>
    <<div class="jumbotron">
        <h1 class="display-4">Welcome to <?php echo $catname;?> Forums</h1>
        <p class="lead"><?php echo substr($catdesc,0, 250);?> ...</p>
        <hr class="my-4">
        <p>This is a peer to peer forum for sharing knowledge with each other.</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
        </p>
        </div>
        <div class="container">
            <h1 class="py-2">Browse Questions</h1>
            <?php
        $id = $_GET['catid'];
         $sql = "SELECT * FROM `threads` WHERE thread_cat_id = $id";
         $result = mysqli_query( $conn, $sql );
         while ( $row = mysqli_fetch_assoc( $result ) ) {
             $id = $row['thread_id'];
             $title = $row['thread_title'];
             $desc = $row['thread_desc'];

         }

         echo ' <div class="media my-3">
         <img class="mr-3" width="50px" src="userdefault.png" alt="Generic placeholder image">
         <div class="media-body">
           <h5 class="mt-0">'.$title.'</h5>'.$desc.'
         </div>
       </div>
            ';
        ?>
            <!-- <div class="media my-3">
      <img class="mr-3" width="50px" src="userdefault.png" alt="Generic placeholder image">
      <div class="media-body">
        <h5 class="mt-0">Unable to install xampp?</h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem vero ipsam temporibus quae. Asperiores, harum.
      </div>
    </div>
    <div class="media my-3">
      <img class="mr-3" width="50px" src="userdefault.png" alt="Generic placeholder image">
      <div class="media-body">
        <h5 class="mt-0">Unable to install xampp?</h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem vero ipsam temporibus quae. Asperiores, harum.
      </div>
    </div>
    <div class="media my-3">
      <img class="mr-3" width="50px" src="userdefault.png" alt="Generic placeholder image">
      <div class="media-body">
        <h5 class="mt-0">Unable to install xampp?</h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem vero ipsam temporibus quae. Asperiores, harum.
      </div>
    </div>
    <div class="media my-3">
      <img class="mr-3" width="50px" src="userdefault.png" alt="Generic placeholder image">
      <div class="media-body">
        <h5 class="mt-0">Unable to install xampp?</h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem vero ipsam temporibus quae. Asperiores, harum.
      </div>
    </div>
    <div class="media my-3">
      <img class="mr-3" width="50px" src="userdefault.png" alt="Generic placeholder image">
      <div class="media-body">
        <h5 class="mt-0">Unable to install xampp?</h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem vero ipsam temporibus quae. Asperiores, harum.
      </div>
    </div>
    <div class="media my-3">
      <img class="mr-3" width="50px" src="userdefault.png" alt="Generic placeholder image">
      <div class="media-body">
        <h5 class="mt-0">Unable to install xampp?</h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem vero ipsam temporibus quae. Asperiores, harum.
      </div>
    </div>
  </div> -->

</body>

</html>

6080270202942586