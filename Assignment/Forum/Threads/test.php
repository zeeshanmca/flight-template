<?php require "./feed.php";
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-25 11:09:51
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-25 11:09:51
 */

include '../_partials/_dbconnect.php';
$method = $_SERVER['REQUEST_METHOD'];
if ($method == 'POST') {
    // insert thread into db
    $th_title = $_POST['title'];
    $th_desc = $_POST['desc'];
    $sql = "INSERT INTO `threads` (`thread_id`, `thread_title`, `thread_desc`, `thread_cat_id`, `thread_user_id`, `timestamp`) VALUES (NULL, '$th_title', '$th_desc', '$id', '2', current_timestamp())";
    $result = mysqli_query($conn, $sql);
    $showAlert = true;
    if ($showAlert) {
        echo '
            <div class="alert alert-success" role = "alert"<strong>Success!</strong> Your thread has been posted. Please wait for community to respond.</div>
        ';
    }
}