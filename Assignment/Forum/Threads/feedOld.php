<?php

/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-08 12:33:22
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-21 17:57:17
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="cardfeed.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="../_partials/style.css">
  <!-- <link rel="stylesheet" href="../Popup/style.css"> -->


  <!-- <title>Document</title> -->
</head>

<body>
  <?php include '../_partials/nav.php';?>

  <!-- DataBase Connect -->
  <?php include '../_partials/_dbconnect.php';?>

  <!-- Getting Category Name and description -->
  <?php
$id = $_GET['catid'];

$sql = "SELECT * FROM catagories WHERE category_id= $id ";
$result = mysqli_query($conn, $sql);
//  echo '<div class="toggles">';
$row = mysqli_fetch_assoc($result);
$catname = $row['category_name'];
$catdesc = $row['category_description'];

?>

  <?php
/*
$id = $_GET['catid'];

$showAlert = false;

$method = $_SERVER['REQUEST_METHOD'];
// echo $method;
// echo $method;
if ($method == 'POST') {
// insert thread into db
$th_title = $_POST['title'];
$th_desc = $_POST['desc'];

$sql = "INSERT INTO `threads` (`thread_title`, `thread_desc`, `thread_cat_id`, `thread_user_id`, `timestamp`) VALUES ('$th_title', '$th_desc', '$id', '0', current_timestamp())";
$result = mysqli_query($conn, $sql);
$showAlert = true;
if ($showAlert) {
echo '
<div class="alert alert-success" role="alert">

<strong>Success!</strong> Your thread has been posted. Please wait for community to respond.
</div>
';
}
}
 */
?>


  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-4">Welcome to <?php echo $catname; ?> Forums</h1>
      <p class="lead"><?php echo substr($catdesc, 0, 250); ?>....</p>
    </div>
  </div>



  <div class="container">
    <h1 class="c-btn">Start Discussion</h1>

    <br>
    <br>
    <br>








    <!-- echo $_SERVER['REQUEST_URI']  -->
    <form  method="POST" id="c3po" >
      <div class="form-group">
        <label for="title">Problem Title</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Keep as short and crisp as possible." required style="width: 400px;
            height: 40px; margin-left: 0;">
      </div>
      <input type="hidden" name="_id" id="_id" value=<?php echo ($_GET['catid']); ?>  />

      <div class="form-group">
        <label for="desc">Problem Description</label>
        <textarea class="form-control" id="desc" rows="3" placeholder="Whether you have questions or concerns, or just need help with something right now, we’re here." name="desc" required style="width: 400px;
            height: 150px; margin-left: 0;"></textarea>

      </div>
      <div class="form-group">
        <button type="submit"  name="submit" class="btn btn-primary">Post</button>
      </div>
    </form>
<script>
  function handleSaveThread(e){

    const title = document.getElementById("title").value;
    const desc = document.getElementById("desc").value;
    const id = document.getElementById("_id").value;
    console.log(id+5);
    let formData = new FormData();
formData.append('title', title);
formData.append('desc', desc);
formData.append('id', id);

fetch("http://localhost/ForumV2/PostFeed/saveThread.php",
    {
        body: formData,
        method: "post"
    }).then((res)=>{
      console.log(res);
    });

    e.preventDefault();
  }
  document.getElementById("c3po").addEventListener('submit',handleSaveThread);

</script>


  </div>

  <div class="container">
    <h1 class="c-btn">Browse Questions</h1>
  </div>


  <!-- Threads -->
  <div class="container group">

    <!-- <section class="box-offset col-2-3 pad-top"> -->
    <ul class="media-primary">

      <?php
$id = $_GET['catid'];
$sql = "SELECT * FROM threads WHERE thread_cat_id = $id";
$noResult = true;

$result = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_assoc($result)) {
    $id = $row['thread_id'];
    $title = $row['thread_title'];
    $desc = $row['thread_desc'];
    $threadTime = $row['timestamp'];
    $noResult = false;
    echo '
        <li class="media col-gutters pad-top">
        <div class="media-body">
          <div class="media pad-top">
            <a class="media-object-alt" href="#">
              <img src="https://s.cdpn.io/42746/darby.jpg" alt="userDefault">
            </a>
            <div class="media-body">
            <strong>Annonymous User at <em> ' . $threadTime . '</em> </strong><br>

              <h5><a href="../Post/post.php?threadid=' . $id . '">
              <strong>' . $title . '</strong>  <span class="text-subtle h-promo"></span>
                <br><br>
              <p><em>' . substr($desc, 0, 100) . '..</em></p></a></h5>
            </div>
          </div>
        </div>
      </li>
        ';
}
// echo var_dump($noResult);
if ($noResult) {
    echo '
        <div class="jumbotron jumbotron-fluid">
          <div class="container">
            <p class="display-4"><strong>No Questions Here.</strong></p>
            <p class="lead">Be the first person to ask a question.</p>
            </div>
          </div>';
}
?>


      <!-- form -->


      <!-- /PopUp -->
      <!-- <script src="../Popup/script.js"></script> -->
      <script>
        window.setTimeout(function() {
          $(".alert").fadeTo(500, 0).slideUp(500, function() {
            $(this).remove();
          });
        }, 2000);
      </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>

</body>

</html>