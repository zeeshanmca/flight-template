<?php

/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-08 09:21:23
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-28 10:08:38
 */
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="_partials/style.css">
    <link rel="stylesheet" href="Categories/style.css">
    <link rel="stylesheet" href="Search/results.css">
    <link href="_partials/alert.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="Catagories/new.css"> -->
    <link rel="stylesheet" href="_partials/font/flaticon.css">
    <link rel="stylesheet" href="_partials/font4/flaticon.css">

    <link rel="stylesheet" href="_partials/fontawesome/css/all.css">
    <!-- <script src="_partials/test.js"></script> -->
    <title>Forum</title>
</head>

<body>

    <!-- Nav Link -->
    <?php include '_partials/nav.php'; ?>


    <!-- DataBase Connect -->
    <?php include '_partials/_dbconnect.php'; ?>

    <!-- Catagories -->
    <?php include 'Categories/Category.php'; ?>

    <!-- Pop Up -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
</body>

</html>
