<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-18 11:06:45
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-05 13:26:42
 */
 ?>

<?php 
    // Script to connect to the DataBase
    $serverName = "localhost";
    $username = "root";
    $password = "";
    $database = "forumv2";

    $conn = mysqli_connect($serverName, $username, $password, $database);
    $salt="C3pO";
    function verifyHash($pass, $hash)
    {
        return $pass==$hash;
    }
?>