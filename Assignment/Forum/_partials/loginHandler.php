<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-22 12:16:25
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-22 13:17:39
 */
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="loginHandler.css">
  <title>Document</title>
</head>
<body>
<header role="banner">
  <h1>Jahangir Ahmad</h1>
  <ul class="utilities">
    <li class="logout warn"><a href="">Log Out</a></li>
  </ul>
</header>

<nav role="navigation">
  <ul class="main">
    <li class="dashboard"><a href="#">Dashboard</a></li>
    <li class="write"><a href="#" disabled>Write Post</a></li>
    <li class="edit"><a href="#" disabled>Edit Posts</a></li>
    <li class="comments"><a href="#" disabled>Comments</a></li>
  </ul>
</nav>

<main role="main">
  <section class="panel important">
    <h2>Welcome to Your Dashboard </h2>
    <ul>
      <li>Edit Post & Edit Comment Functionalities Will Be Added In Future.</li>
      <li>Aliquam tincidunt mauris eu risus.</li>
      <li>Vestibulum auctor dapibus neque.</li>
    </ul>
  </section>
  <section class="panel">
    <h2>Your Information</h2>
    <ul>
      <li>Name : Jahangir Ahmad</li>
      <li>Enrollment Number : 18045110049</li>
      <li>Department : Computer Science</li>
      
    </ul>
  </section>
  <section class="panel">
    <h2>Posts & Comments</h2>
    <ul>
      <li><b>2458 </b>Published Posts</li>
      <li><b>18</b> Comments.</li>
      <li>Most popular post: <b>This is a post title</b>.</li>
    </ul>
  </section>
  
  <section class="panel important">
    <h2>Write a feedback</h2>
    <form action="#">
      <div class="twothirds">
        <label for="name"></label>
        <input type="text" name="name" id="name" placeholder="Jahangir Ahmad" disabled />
        <br>
        <div>
        <input type="text" name="name" id="name" placeholder="18045110049"  disabled/>
          <br>
        </div>
        <div>
        <input type="text" name="name" id="name" placeholder="Computer Science" disabled/>
          <br>
        </div>

        <label for="textarea">Your Feeback:</label>
        <textarea cols="40" rows="8" name="textarea" id="textarea"></textarea>
        <div>
          <input type="submit" value="Submit" />
        </div>
      </div>
    </form>
  </section>
</main>
<footer role="contentinfo">Jahangir Ahmad</footer>
</body>
</html>