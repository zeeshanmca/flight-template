<?php

/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-08 09:12:39
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-29 07:19:09
 */

session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="theme-color" content="#B71C1C">

    <link href="alert.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="new.css">
    <link href='style.css' type='text/css' rel='stylesheet' />
    <!-- <link rel="stylesheet" href="fontawesome/css"> -->

    <link rel="stylesheet" href="font/flaticon.css">
    <link rel="stylesheet" href="font4/flaticon.css">
    <!-- <script src="fontawesome/js/all.js"></script> -->
    <!-- <script src="test.js"></script> -->

    <title>Header</title>
</head>

<body>

    <header>
        <div class='container'>
            <div class='mobile_nav'>
                <button class='burger' title='Open and close menu'>
                    <span class='mobile_nav__label'>Open and close menu</span>
                    <div class='top stripe'></div>
                    <div class='middle stripe'></div>
                    <div class='bottom stripe'></div>
                </button>
            </div>
            <div class='mobile_menu'>
                <nav>
                    <ul>
                        <li class=''>
                            <a href='http://localhost/Forum/index.php'>Category</a>
                        </li>
                        <li>
                            <a href="http://localhost/Forum/Error/error.html">About</a>
                        </li>
                        <li>
                            <a href='http://localhost/Forum/Contact/contact.html'>Contact</a>
                        </li>

                    </ul>
                </nav>
            </div>
            <div class='logo'>
                <a href='http://localhost/Forum/index.php'><span>Forum</span></a>
                <!-- <span>Forum</span>  -->
            </div>
            <nav class='head_nav'>
                <ul>
                    <li class=''>
                        <a href='http://localhost/Forum/index.php'>Category</a>
                    </li>
                    <li>
                        <a href='http://localhost/Forum/Error/error.html'>About</a>
                    </li>
                    <li>

                        <a href='http://localhost/Forum/Contact/contact.html'>Contact</a>
                    </li>
                    <?php

                    if (isset($_SESSION['user_id'])) {
                    }
                    ?>
                </ul>
            </nav>

            <div class='icons'>

                <?php

                if (isset($_SESSION['user_id'])) {
                ?>
                    <div class='block'>
                        <a class='user_profile' href='http://localhost/Forum/_partials/logout.php ' title='My Account'>
                            <i class="flaticon-man"></i>


                        </a>

                    </div>
                    <div class='block'>
                        <a class='user_profile' href='http://localhost/Forum/_partials/logout.php ' title='Logout'>
                            <i class="flaticon-logout"></i>

                        </a>

                    </div>

                <?php

                } else {
                ?>
                    <div class='block'>
                        <a class='user_profile' href="http://localhost/Forum/LoginSignup/login.php" title='User profile'>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 299.997 299.997">
                                <path d="M149.996,0C67.157,0,0.001,67.158,0.001,149.997c0,82.837,67.156,150,149.995,150s150-67.163,150-150    C299.996,67.156,232.835,0,149.996,0z M150.453,220.763v-0.002h-0.916H85.465c0-46.856,41.152-46.845,50.284-59.097l1.045-5.587    c-12.83-6.502-21.887-22.178-21.887-40.512c0-24.154,15.712-43.738,35.089-43.738c19.377,0,35.089,19.584,35.089,43.738    c0,18.178-8.896,33.756-21.555,40.361l1.19,6.349c10.019,11.658,49.802,12.418,49.802,58.488H150.453z"></path>
                            </svg>
                        </a>
                    </div>
            </div>
        </div>
    <?php
                }
    ?>
    </header>

    <?php
    // include "./LoginSignup/login.php";
    if (isset($_GET['signupsuccess']) && $_GET['signupsuccess'] == "true") {
        echo '
    <div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Success!</strong> You can log in now.

</div>
    ';
    }

    ?>
    <?php
    // include "./LoginSignup/login.php";
    if (isset($_GET['loginsuccess']) && $_GET['loginsuccess'] == "true") {
        echo '
    <div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Success!</strong> You are logged in now.

</div>
    ';
    }


    if (isset($_GET['logoutsuccess']) && $_GET['logoutsuccess'] == "true") {
        echo '
  <div class="alert alert-success alert-dismissible fade show" role="alert">
<strong>Success!</strong> You are logged out now.

</div>
  ';
    }
    if (isset($_GET['loginsuccess']) && $_GET['loginsuccess'] == "false") {
        echo '
  <div class="alert alert-success alert-dismissible fade show" role="alert">
<strong>Oops!</strong> Invalid Credentials.

</div>
  ';
    }
    if (isset($_GET['alreadyexists']) && $_GET['alreadyexists'] == "true") {
        echo '
  <div class="alert alert-success alert-dismissible fade show" role="alert">
<strong>Oops!</strong> Email Already In Use.

</div>
  ';
    }

    if (isset($_GET['contactus']) && $_GET['contactus'] == "true") {
        echo '
        <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Thank you</strong> for contacting us, we will get back to you soon.

        </div>
        ';
    }
    ?>
    <script>
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 3000);
    </script>

    <script src="script.js"></script>
    <script src="prototype4.js"></script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js"></script> -->

    <script src="../GlobalAssets/jQuery.js"></script>
    <script src="../GlobalAssets/jqueryui.js"></script>
</body>

</html>
