<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-09 09:04:41
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-27 09:35:12
 */
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="results.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js"></script>

    <title>Document</title>
</head>

<body>
    <br>
    <form id="formbase" action = "./Search/results.php" method = "get">
        <label class="searchbox">
            <input class="searchfield" type="search" name = "search" placeholder="Search" checked="checked" />
        </label>
    </form>
</body>

</html>