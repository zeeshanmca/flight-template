<?php

/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-21 18:13:14
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-23 12:35:31
 */

$showError = "false";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include '../_partials/_dbconnect.php';
    $email = $_POST['loginEmail'];
    $pass = $_POST['loginPass'];
    $sql = "SELECT * FROM `usersignup` WHERE user_email = '$email'";
    $result = mysqli_query($conn, $sql);
    $numRows = mysqli_num_rows($result);
    if ($numRows == 1) {
        $row = mysqli_fetch_assoc($result);
        if (password_verify($pass, $row['user_pass'])) {
            session_start();
            $_SESSION['loggedin'] = true;
            $_SESSION['useremail'] = $email;
            // echo "logged in" . $email;  
            header("Location: ../index.php?loginsuccess=true");
            exit;
            
        }
        else {
            $showError = "Login Failed.";
        }
        header("Location: ../index.php?signinsuccess=false&error=$showError");
        
    }
}
