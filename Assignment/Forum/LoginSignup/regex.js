/*
 * @Author: Jahangir Ahmad
 * @Date:   2020-08-13 14:34:47
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-13 14:34:47
 */
$(function () {
    // var regExp = /^\w*(\.\w*)?@\w*\.[a-z]+(\.[a-z]+)?$/;
    var regExp = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;

    $('[type="email"]').on("keyup", function () {
        $(".message").hide();
        regExp.test($(this).val())
            ? $(".message.success").show()
            : $(".message.error").show();
    });
});
