<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-08-28 14:12:48
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-28 14:54:11
 */
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="login.css" />
    <title>Document</title>
</head>
<body>
    <form action="_handleSignup.php" method="POST">
        <h1>Create Account</h1>

        <label>Name</label>
        <input type="text" name = "signupName" autocomplete="off"
         pattern=".{10,30}" 
         />
        <span data-validation-text>Must be 10 chars or more</span>

        <label>Email</label>
        <input type="email" name = "signupEmail" autocomplete="off" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" />
        <span data-validation-text>Must be a valid email</span>

        <label>Ernollment Number</label>
        <input type=""  name = "ENo" autocomplete="off" pattern="([0-9]){11,11}" />
        <span data-validation-text>Must be 11 chars, only numbers allowed</span>

        <label>Department</label>
        <input type="text" name="userDept" autocomplete="off" pattern=".{10,}" />
        <span data-validation-text>Must be 10 chars or more</span>
        <label>Password</label>
        <input type="password" name="signupPassword" autocomplete="off" pattern="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\S{6,}" />
        <span data-validation-text>Password must contain at least 6 chars and at least 1 number and 1 Capital letter
        </span>
        <label>Confirm Password</label>
        <input type="password" name="signupcPassword" autocomplete="off" pattern="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])\S{6,}" />
        <span data-validation-text>Password must contain at least 6 chars and at least 1 number and 1 Capital letter
        </span>
        <!-- <label>Phone</label>
        <input type="text" pattern="([0-9]){10,10}" />
        <span data-validation-text>Must be 10 chars , only numbers are allowed</span> -->
        <p><button class="jui-card1__button" style="text-align: center; justify-content: center8;">Sign up</button></p>
        <p>Already have an account,
                <button type="button" style="" onClick="window.location='login.php';"> Sign in</button></a></p>
        </p>
    </form>
</body>
</html>
