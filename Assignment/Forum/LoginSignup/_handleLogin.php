<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-24 08:36:11
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-28 16:23:12
 */
?>
<?php
ob_start();
session_start();
$login = false;
$showError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include '../_partials/_dbconnect.php';
    $username = $_POST['loginEmail'];
    $password = sha1($_POST['loginPass'] . $salt);

    $sql = "SELECT * FROM users WHERE user_email ='$username' AND user_pass = '$password'";

    $result = mysqli_query($conn, $sql);
    $num = mysqli_num_rows($result);

    if ($num > 0) {
        $row = mysqli_fetch_assoc($result);
        if (verifyHash($password, $row['user_pass'])) {
            $login = true;
            $showError = "Logged In";
            echo 'Logged In';
            $_SESSION['user_id'] = $row['user_name'];
            $_SESSION['userID'] = $row['sno'];
            header("location: ../index.php?loginsuccess=true");
        } else {
            $showError = "Invalid credentials";
            echo 'Invalid';
        }
    }else
    {
        header("location: ../index.php?loginsuccess=false");
    }
}
