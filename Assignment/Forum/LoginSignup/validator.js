/*
 * @Author: Jahangir Ahmad
 * @Date:   2020-08-13 13:49:17
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-13 13:57:21
 */
var emailInput;

$("#email-input").on("change", function() {
  emailInput = $(this).val();

  if (validateEmail(emailInput)) {
    $(this).css({
      color: "white",
      background: "green",
      border: "1px solid green"
    });
  } else {
    $(this).css({
      color: "red",
      border: "1px solid red"
    });

    // alert("not a valid email address");
  }
});

$("#submit").on("click", function(e) {
  // e.preventDefault();
  if (validateEmail(emailInput)) {
    alert("you did it!");
  } else {
    alert('nope');
    return false;
  }
});

function validateEmail(email) {
  var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

  return $.trim(email).match(pattern) ? true : false;
}
