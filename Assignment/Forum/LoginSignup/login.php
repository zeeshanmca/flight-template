<?php
/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-08-28 14:07:32
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-28 14:12:19
 */
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="login.css">
    <!-- <link rel="stylesheet" href="../Styles/style.css"> -->
    <title>Document</title>
</head>
<body>
    <form action="_handleLogin.php" method="POST" style="
            max-width: 360px;
            width: 100%;
            padding: 58px 44px;
            border: 1px solid #e1e2f0;
            border-radius: 4px;
            box-shadow: 0 0 5px 0 rgba(42, 45, 48, 0.12);
            transition: all 0.3s ease;">
        <h1>Sign in</h1>

        <div class="row">
            <label for="email">Email</label>
            <input type="email" name="loginEmail" placeholder="email@example.com">
        </div>

        <div class="row">
            <label for="password">Password</label>
            <input type="password" name="loginPass">
        </div>
        <br>
        <div class="row">
            <button type="submit">Sign in</button>
        </div>
        <div class="row">

        <p>Don't have an account,
                <button type="button" style="" onClick="window.location='signup.php';"> Sign up</button></a></p>
        </div>
    </form>
</body>
</html>
