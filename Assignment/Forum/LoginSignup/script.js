/*
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-08 08:05:28
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-08 08:23:27
 */
const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
    container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
    container.classList.remove("right-panel-active");
});