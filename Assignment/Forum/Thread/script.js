/*
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-08 11:09:26
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-08 11:09:26
 */
function resizeHeaderOnScroll() {
    const distanceY = window.pageYOffset || document.documentElement.scrollTop,
        shrinkOn = 200,
        headerEl = document.getElementById('js-header');

    if (distanceY > shrinkOn) {
        headerEl.classList.add("smaller");
    } else {
        headerEl.classList.remove("smaller");
    }
}

window.addEventListener('scroll', resizeHeaderOnScroll);
