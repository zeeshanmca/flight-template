<?php

/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-20 09:37:50
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-07-20 09:40:40
 */

// require "post.php";
?>

<?php

// <!-- Insert into comment db -->

$showAlert = false;
$method = $_SERVER['REQUEST_METHOD'];
// echo $method;
// echo $method;
if ($method == 'POST') {
    // insert into comment db
    $comment = mysqli_real_escape_string($conn, $_POST['comment']);
    $sql = "INSERT INTO comments (`comment_content`, `thread_id`, `comment_by`, `comment_time`) VALUES ('$comment', '$id', '0', current_timestamp())";
    $result = mysqli_query($conn, $sql);
    $showAlert = true;
    if ($showAlert) {
        echo '
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Your comment has been added.
</div>
';

        header("Location: post.php");
        exit;
    }
}
?>

