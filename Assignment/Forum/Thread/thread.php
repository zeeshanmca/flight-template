<?php

/**
 * @Author: Jahangir Ahmad
 * @Date:   2020-07-08 11:59:07
 * @Last Modified by:   Jahangir Ahmad
 * @Last Modified time: 2020-08-29 07:27:58
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../_partials/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.8.0/angular.min.js"></script>
    <script src="../Comment/script.js"></script>
    <link rel="stylesheet" href="../Comment/style.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="warn.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="comment.css">
    <link rel="stylesheet" href="../_partials/font/flaticon.css">
    <link rel="stylesheet" href="../_partials/font4/flaticon.css">



    <!-- <title>?</title> -->
</head>

<body>
    </head>

    <body>
        <!-- Nav -->
        <?php include "../_partials/nav.php"; ?>
        <?php include "../_partials/_dbconnect.php"; ?>

        <!-- /Nav -->
        <?php
    $id = $_GET['threadid'];
    
    $sql = "SELECT * FROM threads WHERE thread_id = $id";
    $result = mysqli_query($conn, $sql);
    //  echo '<div class="toggles">';
    while ($row = mysqli_fetch_assoc($result)) {
        $title = $row['thread_title'];
        $desc = $row['thread_desc'];
        $threadTime = $row['timestamp'];
        $thread_user_id = $row['thread_user_id'];
          $sqlTest = "SELECT user_name FROM users WHERE sno =  '$thread_user_id'";
          $resultTest = mysqli_query($conn, $sqlTest);
          $rowTest = mysqli_fetch_assoc($resultTest);
          $user_name = $rowTest['user_name'];
    }
    ?>

        <!-- Insert into comment db -->
        <?php
    $showAlert = false;

    $method = $_SERVER['REQUEST_METHOD'];
    // echo $method;
    // echo $method;
    if (isset($_SESSION['userID'])) {
        $cuid = $_SESSION['userID'];
          
    }
    // $cuid=$_SESSION['userID'];
    // $cuid=(isset($_GET['userID']) ? intval($_GET['userID']) : -1);
    if ($method == 'POST') {
        // insert  into comment db
        $comment = mysqli_real_escape_string($conn, $_POST['comment']);
        $sql = "INSERT INTO comments (`comment_content`, `thread_id`, `user_id`, `comment_time`) VALUES ('$comment', '$id',$cuid , current_timestamp())";
        $result = mysqli_query($conn, $sql);
        $showAlert = true;
        if ($showAlert) {
            echo '
              <div class="alert alert-success" role="alert">
  
  <strong>Success!</strong> Your comment has been added.
</div>
              ';
            // header("Location: post.php??threadid=$id");
        // exit;
        }
    }
    ?>


        <div class="container py-5">
            <div class="jumbotron jumbotron-fluid p-3">
                <h1 class="display-4"><?php echo $title; ?>
                </h1>
                <p class="lead"><strong>Posted by : <?php echo "$user_name"?> </strong></p>
                <div>
                    <p class="lead"><strong>Date : <em><?php echo $threadTime; ?></em> </strong></p>
                </div>

                <hr class="my-4">
                <p><?php echo $desc; ?>
                </p>
            </div>
        </div>



        <!-- form -->


        <!-- /PopUp -->
        <!-- <script src="../Popup/script.js"></script> -->
        <div class="container">
            <h2 class="c-btn">Post a comment.</h2>
        </div>

        <?php
      if (isset($_SESSION['user_id'])) {
          echo '
    
    <div class="container">
     
      <form action="'.$_SERVER["REQUEST_URI"]. '"  method="POST">

        <div class="form-group">
          <label for="comment">Type your comment.</label>
          <textarea class="form-control" id="comment" rows="3" placeholder="Whether you have questions or concerns, or just need help with something right now, we’re here." name="comment" required style="width: 400px;
            height: 150px; margin-left: 0;"></textarea>

        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Post</button>
        </div>
      </form>
    </div>';
      } else {
          echo '
        <div class="container">
        <div id="InfoBanner" style="">
        <span class="reversed reversedRight">
          <span>
            &#9888;
          </span>
        </span>
        <span class="reversed reversedLeft">
          Warning you need to login to comment a post !!
        </span> 
      </div>
        </div>
        <div class="container">
     
      <form action="'.$_SERVER["REQUEST_URI"]. '"  method="POST">

        <div class="form-group">
          <label for="comment">Type your comment.</label>
          <textarea class="form-control" id="comment" rows="3" placeholder="Whether you have questions or concerns, or just need help with something right now, we’re here." name="comment" required disabled style="width: 400px;
            height: 150px; margin-left: 0;"></textarea>

        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" disabled>Post</button>
        </div>
      </form>
    </div>
        
    ';
      }
    ?>
        <!-- Comment -->
        <div class="container">
            <h3 class="c-btn">Discussions</h3>
        </div>
        <div class="container group" id="ques">

            <!-- <section class="box-offset col-2-3 pad-top"> -->
            <ul class="media-primary">

                <?php
        $id = $_GET['threadid'];
        $sql = "SELECT * FROM comments WHERE thread_id = $id";
        $noResult = true;

        $result = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_assoc($result)) {
            $id = $row['comment_id'];
            $content = $row['comment_content'];
            $comment_by = $row['user_id'];
            $time = $row['comment_time'];
            $noResult = false;
            $sql2 = "SELECT user_name FROM users WHERE sno =  $comment_by";
            $result2 = mysqli_query($conn, $sql2);
            $row2 = mysqli_fetch_assoc($result2);
            $user_name = $row2['user_name'];
            echo '
        <li class="media col-gutters pad-top">
        <div class="media-body">
          <div class="media pad-top">
            <a class="media-object-alt" href="#">
              <img src="https://s.cdpn.io/42746/darby.jpg" alt="userDefault">
            </a>
            <div class="media-body"><p>
            <strong>' . $user_name . ' </strong><br>
            <strong>' . $time . '</strong>
            <br><br>
            <em style="font-size:14px;">' . $content . '</em>
              </p>
              
            </div>
          </div>
        </div>
      </li>
        ';
        }
        // echo var_dump($noResult);
        if ($noResult) {
            echo '
        <div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h3 class="display-4">No Comments Here</h3>
    <p class="lead">Be the first person to answer a question.</p>
  </div>
</div>';
        }
        ?>


                <!-- /Comment -->
                <script>
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function() {
                        $(this).remove();
                    });
                }, 4000);
                </script>

                <script>
                $(document).ready(function() {
                    window.history.replaceState('', '', window.location.href)
                });
                </script>
                <script src="../GlobalAssets/jQuery.js"></script>
                <script src="../GlobalAssets/jqueryui.js"></script>


    </body>

</html>